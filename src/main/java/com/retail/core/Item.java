/**
 * 
 */
package com.retail.core;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author gyelwand
 *
 */
@XmlRootElement(name="itemsList")
@XmlAccessorType(XmlAccessType.FIELD)
public class Item implements Serializable,Comparable<Item>{

		/**
	 * 
	 */
	private static final long serialVersionUID = 8067624064298637092L;
		/**
	 * 
	 */
	
	
	private String upc;
	private String description;
	private double price;
	private double weight;
	private String shippingMethod;
	private double shippingCost;
	
	@XmlElement(name = "itemList")
    private List<Item> items = null;
	
	
	
	/**
	 * @return the items
	 */
	public List<Item> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}

	public Item() {}  
	
	/**
	 * @return the shippingCost
	 */
	public double getShippingCost() {
		return shippingCost;
	}

	/**
	 * @param shippingCost the shippingCost to set
	 */
	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}

	//constructor to initialize the items 
	public Item(String upc, String description, double price, double weight, String shippingMethod) throws IllegalArgumentException{
		if(weight<0) {
			throw new IllegalArgumentException("negative weight not allowed");
		}
		
		if(description==null) {
			throw new IllegalArgumentException("null value for item description is not allowed");
		}
		
		if(!(shippingMethod.equalsIgnoreCase("AIR") ||  shippingMethod.equalsIgnoreCase("GROUND") 
				  || shippingMethod.equalsIgnoreCase("RAIL"))) {
			throw new IllegalArgumentException("Invalid Shipping Method");
		}
		
		this.upc=upc;
		this.description=description;
		this.price=price;
		this.weight=weight;
		this.shippingMethod=shippingMethod;
		
	}
	
	/**
	 * @return the upc
	 */

	public String getUpc() {
		return upc;
	}
	/**
	 * @param upc the upc to set
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}
	/**
	 * @return the description
	 */
	
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the price
	 */

	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	/**
	 * @return the shippingMethod
	 */

	public String getShippingMethod() {
		return shippingMethod;
	}
	/**
	 * @param shippingMethod the shippingMethod to set
	 */
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	
	public int compareTo(Item itemObject) {
		//int oldUpc=Integer.par(this.upc);
		
		BigInteger oldUPC=new BigInteger(this.upc);
		BigInteger newUPC=new BigInteger(itemObject.getUpc());
        return newUPC.compareTo(oldUPC);
    }		
}
