package com.retail.core;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

public class RailShippingStrategy implements ShippingStrategy{
	//Items that weigh less than 5.0 pounds cost $5 to ship.  Items that weigh more than 5.0 pounds cost $10 to ship 
	static final Logger logger = Logger.getLogger(RailShippingStrategy.class);
	//This method is used to calculate the shipping cost of Item for Air strategy
	public void calculateShippingCost(Item item) {
	
	 double itemWeight=item.getWeight();
	 
	 try {	
		//Calculate the shipping cost and add shippingTariff of item 
		 double shippingCost=0.0;
		 
		 if(itemWeight<5.0) {
			 shippingCost=5.0;
		 }
		 else if(itemWeight>5.0) {
			 shippingCost=10.0;
		 }
		System.out.println("shippingCost--------->"+shippingCost);
		item.setShippingCost(shippingCost);
	 }
	 catch(Exception e) {
		 logger.error(e.getMessage());
	 }
  }
}
