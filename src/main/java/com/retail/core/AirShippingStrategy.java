/**
 * 
 */
package com.retail.core;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

/**
 * @author gyelwand
 *
 */
public class AirShippingStrategy implements ShippingStrategy {
	
	static final Logger logger = Logger.getLogger(AirShippingStrategy.class);
	//This method is used to calculate the shipping cost of Item for Air strategy
	public void calculateShippingCost(Item item) {
	
		double itemWeight=item.getWeight();
		String upcCodeStr=item.getUpc();
	 try {	
		//calculates the shippingTariff of cost of item
		double shippingTeriffOfItem=(item.getPrice()* 0.03);
		
		//get the second last digit of UPC code 
		String tempStringwithLastTwoChar=upcCodeStr.substring(upcCodeStr.length() - 2);
		String finalString=tempStringwithLastTwoChar.substring(0, 1);
		
		//Calculate the shipping cost and add shippingTariff of item 
		double shippingCost=itemWeight * Integer.parseInt(finalString);
		//not used
		//shippingCost+=shippingTeriffOfItem;
		
		DecimalFormat f = new DecimalFormat("##.##");
		String shippingCostStr=f.format(shippingCost);
		 
		shippingCost=Double.parseDouble(shippingCostStr);
		
		item.setShippingCost(shippingCost);
	 }
	 catch(Exception e) {
		 logger.error(e.getMessage());
	 }
		
	}
}
