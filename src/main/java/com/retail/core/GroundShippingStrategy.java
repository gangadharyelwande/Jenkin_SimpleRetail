/**
 * 
 */
package com.retail.core;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

/**
 * @author gyelwand
 *
 */
public class GroundShippingStrategy implements ShippingStrategy {
	
	static final Logger logger = Logger.getLogger(GroundShippingStrategy.class);
	//This method is used to calculate the shipping cost of Item for Ground method type
	public void calculateShippingCost(Item item) {
		
		try {
			
		
		double itemWeight=item.getWeight();
		
		double shippingCost=(itemWeight * 2.5);
		
		DecimalFormat f = new DecimalFormat("##.##");
		String shippingCostStr=f.format(shippingCost);
		 
		shippingCost=Double.parseDouble(shippingCostStr);
		
		item.setShippingCost(shippingCost);
		}
		
		catch(Exception e) {
			 logger.error(e.getMessage());
		 }
	}

}
