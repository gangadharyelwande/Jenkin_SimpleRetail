/**
 * 
 */
package com.retail.core;


/**
 * @author gyelwand
 *
 */
//This interface makes use of Strategy design pattern
public interface ShippingStrategy{
	
	public void calculateShippingCost(Item item);

}
