import static org.junit.Assert.*;


import org.junit.Test;


import com.retail.core.AirShippingStrategy;
import com.retail.core.GroundShippingStrategy;
import com.retail.core.Item;
import com.retail.core.RailShippingStrategy;


public class ShipmentCrtTest{
	Item item;

	@Test
	public void checkObjectIsNullTest() {
		System.out.println("in checkObjectIsNullTest--->");
		Item itemObject1= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		Item itemObject2= itemObject1;
		
		assertEquals(itemObject1,itemObject2);
	}
	
	@Test
	public void checkObjectIsEqualTest() {
		System.out.println("in checkObjectIsEqualTest--->");
		Item itemObject1= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		Item itemObject2= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		
		assertEquals(itemObject1,itemObject2);
	}
	
	/*@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void negativeWeightTest() {
		  System.out.println("in negativeWeightTest--->");
		  exception.expect(IllegalArgumentException.class);
		  exception.expectMessage("negative weight not allowed");
		  item= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,-0.58, "AIR");
		
	}*/
	
	/*@Test
	public void nullDescriptionTest() {
		  System.out.println("in nullDescriptionTest--->");
		  exception.expect(IllegalArgumentException.class);
		  exception.expectMessage("null value for item description is not allowed");
		  item= new Item("567321101987",null,19.99,0.58, "GROUND");
	}*/
	
	/*@Test
	public void invalidShippingMethodTest() {
		  System.out.println("in invalidShippingMethodTest--->");
		  exception.expect(IllegalArgumentException.class);
		  exception.expectMessage("Invalid Shipping Method");
		  item= new Item("567321101987","CD � Michael Jackson, Thriller",19.99,0.58, "GROUNDANDAIR");
	}*/
	
	/*@Test
	public void invalidShippingCostTest() {
		  System.out.println("in invalidShippingCostTest--->");
		  exception.expect(IllegalArgumentException.class);
		  exception.expectMessage("Invalid Shipping Method");
		  item= new Item("567321101987","CD � Michael Jackson, Thriller",19.99,0.58, "RAIL");
	}*/
	
	@Test
	public void upcLengthTest() {
		  System.out.println("in upcLengthTest--->");
		  item= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		  assertEquals(12, item.getUpc().length());
	}
	
	@Test
	public void groundShippingStrategyCostTest() {
		  System.out.println("in groundShippingStrategyCostTest--->");
		  Item itemObject2= new Item("567321101986","CD � Beatles, Abbey Road",17.99,0.61, "GROUND");
		  GroundShippingStrategy groundShippingObj = new GroundShippingStrategy();
		  
		  groundShippingObj.calculateShippingCost(itemObject2);
		  
		  System.out.println("itemObject2.getShippingCost() is-->"+itemObject2.getShippingCost());
		  assertEquals(1.52,itemObject2.getShippingCost(),0.001);
	}
	
	@Test
	public void airStrategyCorrectCostTest() {
		  System.out.println("in airStrategyCorrectCostTest--->");
		  Item itemObject= new Item("312321101516","Hot Tub",9899.99,793.41, "RAIL");
		  RailShippingStrategy airShippingObj = new RailShippingStrategy();
		  
		  airShippingObj.calculateShippingCost(itemObject);
		  
		  System.out.println("itemObject2.getShippingCost() is-->"+itemObject.getShippingCost());
		  assertEquals(10.0,itemObject.getShippingCost(),0.001);
	}
	
	@Test
	public void airStrategyWrongCostTest() {
		  System.out.println("in airStrategyWrongCostTest--->");
		  Item itemObject= new Item("312321101516","Hot Tub",9899.99,793.41, "RAIL");
		  RailShippingStrategy airShippingObj = new RailShippingStrategy();
		  
		  airShippingObj.calculateShippingCost(itemObject);
		  
		  System.out.println("itemObject.getShippingCost() is-->"+itemObject.getShippingCost());
		  assertEquals(12.0,itemObject.getShippingCost(),0.001);
	}
}
